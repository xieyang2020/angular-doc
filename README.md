### 环境
> 1. [NodeJs](https://nodejs.org/en/)(安装参照 http://jingyan.baidu.com/article/73c3ce28f34b8ce50243d95e.html)

---

### 使用
> 1. 第一次没有安装依赖环境时执行  `` npm run install ``
> 2. 若已经安装了依赖的环境时执行  `` npm run server ``

---

### 内容
> 1. 模块概念
> 2. 服务注入
> 2. 根据页面布局设计路由

---

### 资料
> 1. [Angular 官方文档](https://docs.angularjs.org/api)