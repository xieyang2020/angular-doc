const Router = require('koa-router'),
      Result = require('../model/result'),
      UserService = require('../service/user.service');

const router = new Router();

router.get('/', function *() {
    this.body = 'server start up';
});

router.post('/login', function *() {
    var user = this.request.body, result;
    user = yield UserService.login(user.username, user.password);
    if(user){
        result = Result.success('登录成功', user);
    }else {
        result = Result.error('登录失败', user);
    }
    this.body = result;
});

module.exports = router;