const app = require('koa')(),
      init = require('./init');

init(app);

app.listen(8080);