const json       = require('koa-json'),
      cors       = require('koa-cors'),
      bodyparser = require('koa-bodyparser');

module.exports = function (app) {
    app
        .use(json())
        .use(cors())
        .use(bodyparser());
};