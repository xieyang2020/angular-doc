const glob = require('glob'),
      path = require('path');

module.exports = function (app) {
    glob(path.resolve(__dirname, '..', 'controller/**/*.js'), function (err, matches) {
        matches.forEach((fileName)=> {
            const router = require(fileName);
            app.use(router.routes()).use(router.allowedMethods());
        })
    });
};