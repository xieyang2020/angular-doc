const router       = require('./router'),
      insertGlobal = require('./global'),
      config       = require('./config');

module.exports = function (app) {
    config(app);
    insertGlobal();
    router(app);
};