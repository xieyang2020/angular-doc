const Result = require('../model/result'),
      Dates  = require('../util/dates');

module.exports = function () {
    global.Result = Result;
    global.Dates = Dates;
};