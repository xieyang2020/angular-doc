/**
 * Result
 *   success: [object Boolean]
 *   code: [object Number]
 *   msg: [object String]
 *   data: [object Object]
 */
class Result {
    constructor(msg = '', data = {}, success = false, code = -1) {
        this.success = success;
        this.msg = msg;
        this.code = code;
        this.data = data;
    }

    setSuccess(msg, data) {
        this.success = true;
        this.msg = msg;
        this.data = data || this.data || {};
    }

    setError(msg, data) {
        this.success = false;
        this.msg = msg;
        this.data = data || this.data || {};
    }

    static success(msg, data) {
        return new Result(msg, data, true, 0);
    }

    static error(msg, data) {
        return new Result(msg, data);
    }
}

module.exports = Result;