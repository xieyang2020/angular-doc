angular.module('app').controller('LoginCtrl', ['$scope', 'LoginSvc', '$state', function ($scope, LoginSvc, $state) {
    $scope.login = function () {
        LoginSvc.login($scope.user).then(function (res) {
            var result = res.data;
            if (result.success) {
                $state.go('dashboard');
            } else {
                alert(result.msg || 'err');
            }
        }, function (err) {
            console.log(err);
        });
    };
}]);