angular.module('app').service('LoginSvc', ['$http', function ($http) {
    return {
        login: function (user) {
            return $http.post('http://localhost:8080/login', user);
        }
    };
}]);