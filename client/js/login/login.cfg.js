angular.module('app').config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('login');

    $stateProvider.state('login', {
        url: '/login',
        templateUrl: './view/login/login.html',
        controller: 'LoginCtrl'
    });
}]);