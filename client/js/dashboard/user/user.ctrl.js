angular.module('app').controller('UserCtrl', ['$scope', 'UserSvc', function ($scope, UserSvc) {
    refreshData();

    $scope.remove = function (id) {
        var isDel = confirm('确认删除?');
        isDel && UserSvc.remove(id).then(function (res) {
            var result = res.data;
            if(result.success){
                refreshData();
            }
            console.log(result);
        });
    };

    function refreshData() {
        UserSvc.list().then(function (res) {
            $scope.users = res.data;
        });
    }
}]);