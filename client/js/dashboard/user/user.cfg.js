angular.module('app').config(['$stateProvider', function ($stateProvider) {


    $stateProvider.state('dashboard.user', {
        url: '/user',
        parent: 'dashboard',
        templateUrl: './view/dashboard/user/user.html',
        controller: 'UserCtrl'
    });
}]);