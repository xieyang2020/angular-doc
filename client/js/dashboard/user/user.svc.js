angular.module('app').service('UserSvc', ['$http', function ($http) {
    return {
        list  : function () {
            return $http.get('http://localhost:8080/users');
        },
        remove: function (id) {
            return $http.delete(`http://localhost:8080/users/${id}/remove`);
        }
    };
}]);