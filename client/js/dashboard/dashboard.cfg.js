angular.module('app').config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.when('/dashboard', '/dashboard/user');

    $stateProvider.state('dashboard', {
        url: '/dashboard',
        templateUrl: './view/dashboard/dashboard.html'
    });
}]);